git clone + shh del git (ssh-keygen si no tenim permisos)
git remote -v
git remote add upstream https://gitlab.com/beto/netswithlinux.git
git fetch upstream master
git checkout master
git merge upstream/master
git push






EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt06


```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

	/system reset-configuration
	/system backup load name="20170317_zeroconf"
	/export

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

	
Trabajaremos con dos vlans:

*122: red pública
*222: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free122"
/interface wireless add ssid="private222" master-interface=wlan1
```



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

	porque en el scrip anterior hemos hecho un: /interface wireless set [ find default-name=wlan1 ] disabled=yes

	Para habilitar/deshabilitar hay que ...
	/interface wireless set [ find default-name=wlan1 ] disabled=yes/no
	
	
	
	/interface wireless set [find name=wlan2] name=wPrivate
	/interface wireless set [find name=wlan2] name=wPublic

## Crear interfaces virtuales y hacer bridges

'''
/interface vlan add name eth4-vlan122 vlan-id=122 interface=eth4
/interface vlan add name eth4-vlan222 vlan-id=222 interface=eth4   

/interface bridge add name=br-vlan122
/interface bridge add name=br-vlan222


/interface bridge port add interface=eth4-vlan122 bridge=br-vlan122
/interface bridge port add interface=eth3 bridge=br-vlan122
/interface bridge port add interface=wPublic  bridge=br-vlan122      


/interface bridge port add interface=eth4-vlan222 bridge=br-vlan222
/interface bridge port add interface=wPrivate   bridge=br-vlan222

/interface print


''' 

### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado


### [ejercicio4] Pon una ip 172.17.122.1/24 a br-vlan122 
y 172.17.222.1/24 a br-vlan222 y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración

	ip address add address=172.17.222.1/24 interface=br-vlan222           
	ip address add address=172.17.122.1/24 interface=br-vlan122    
	/ip address print  
	/system backup save name="20170321_config"

	ip a a 172.17.122.22/24 dev enp2s0 para puerto 3
	
	
	para el puerto 4(vlans):
	ip link add link enp2s0 name vlan222 type vlan id 222
	ip link add link enp2s0 name vlan122 type vlan id 122
	
	ip a
	ip a a 172.17.222.22/24 dev vlan222
	ip a a 172.17.122.22/24 dev vlan122
	ip link set vlan222 up
	ip link set vlan122 up
	ping 172.17.122.1
	ping 172.17.222.1
	

### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 
	
	ip pool add ranges=172.17.122.100-172.17.122.200 name=rang_public
	ip pool add ranges=172.17.222.100-172.17.222.200 name=rang_private
	ip dhcp-server network add addres-pool=rang_public
	ip dhcp-server network add addres-pool=rang_private
	ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.122.1 netmask=24 domain=MTK06.com address=172.17.122.0/24 (pub)
	ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.222.1 netmask=24 domain=MTK06.com address=172.17.222.0/24 (priv)
	ip dhcp-server set 0 disabled=no
	ip dhcp-server set 1 disabled=no


#NAT#
/ip address add address=192.168.3.222/16 interface=eth1 disabled=no 
/ip firewall nat add action=masquerade chain=scrnat out-interface=eth1
/ip route add gateway=192.168.0.1
/ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1
/ip firewall filter add chain=input action=accept protocol=tcp dst-port=22 in-interface=eth1 place-before=0



### [ejercicio6] Activar redes wifi y dar seguridad wpa2

	interface wireless set ssid=mktf22
	interface wireless set ssid=mktp22
	interface wireless set 0 disabled=no
	interface wireless set 1 disabled=no
	
	interface wireless security-profiles add name=private wpa2-pre-shared-key=mktp22 (añadir perfiles)
	interface wireless security-profiles set wpa2-pre-shared-key=mktf22 (editar perfiles)
	interface wireless set 1 security-profile=private (asignar perfil)
	
	interface wireless security-profiles set 1 authentication-types=wpa2-psK	}
	interface wireless security-profiles set 1 mode=static-keys-required 		}Activar contraseña
	
	

	
	/interface wireless> print
Flags: X - disabled, R - running
 0    name="wPrivate" mtu=1500 l2mtu=1600 mac-address=6E:3B:6B:BC:C0:27 arp=enabled interface-type=virtual-AP master-interface=wPublic ssid="wPrivate22"
      vlan-mode=no-tag vlan-id=1 wds-mode=disabled wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes
      default-forwarding=yes default-ap-tx-limit=0 default-client-tx-limit=0 hide-ssid=no security-profile=default

 1    name="wPublic" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:BC:C0:27 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="mktp22" frequency=auto
      band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled
      wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0


	gs: * - default
 0 * name="default" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="mktf22"
     supplicant-identity="MikroTik" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password=""
     static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3=""
     static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no
     radius-eap-accounting=no interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled
     group-key-update=5m management-protection=disabled management-protection-key=""

 1   name="private" mode=none authentication-types="" unicast-ciphers=aes-ccm group-ciphers=aes-ccm wpa-pre-shared-key="" wpa2-pre-shared-key="mktp22"
     supplicant-identity="mkt06" eap-methods=passthrough tls-mode=no-certificates tls-certificate=none mschapv2-username="" mschapv2-password=""
     static-algo-0=none static-key-0="" static-algo-1=none static-key-1="" static-algo-2=none static-key-2="" static-algo-3=none static-key-3=""
     static-transmit-key=key-0 static-sta-private-algo=none static-sta-private-key="" radius-mac-authentication=no radius-mac-accounting=no
     radius-eap-accounting=no interim-update=0s radius-mac-format=XX:XX:XX:XX:XX:XX radius-mac-mode=as-username radius-mac-caching=disabled
     group-key-update=5m management-protection=disabled management-protection-key=""


	
	
### [ejercicio6b] Opcional (monar portal cautivo)

	
### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado



