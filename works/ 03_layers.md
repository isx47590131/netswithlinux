1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

	# ip r f all
	# ip a f dev enp2s0
	# ip a 
	# ip r

Ponte las siguientes ips en tu tarjeta ethernet: 2.2.2.2/24, 3.3.3.3/16, 4.4.4.4/25

	# ip a a 2.2.2.2/24 dev enp2s0
	# ip a a 3.3.3.3/16 dev enp2s0
	# ip a a 4.4.4.4/25 dev enp2s0

Consulta la tabla de rutas de tu equipo

	ip r

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132


	2.2.2.2 	-> correcto
	2.2.2.254 	-> Destination Host Unreachable
	2.2.5.2 	-> Network is unreachable
	3.3.3.35	-> Destination Host Unreachable
	3.3.200.45 	-> Destination Host Unreachable
	4.4.4.8 	-> Destination Host Unreachable
	4.4.4.132 	-> Destination Host Unreachable


2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	ip link set enp2s0 down

Conecta una segunda interfaz de red por el puerto usb
Cambiale el nombre a usb0

Modifica la dirección MAC

	ip link set usb0 down
	ip link set usb0 address 00:11:22:33:44:55
	ip link set usb0 name usb3
	ip link set usb3 up

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.
	
	
	# ip a a 5.5.5.5/24 dev usb3
	# ip a a 7.7.7.7/24 dev enp2s0
Observa la tabla de rutas
	ip r



3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet
	
	# ip a f dev enp2s0
	# dhclient -r
	
En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	ip a a 172.16.99.22/24 dev enp2s0
	
Lanzar iperf en modo servidor en cada ordenador

	iperf -s

Comprueba con netstat en qué puerto escucha
	
	netstat -utlnp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:45885           0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::111                  :::*                    LISTEN      -                   
tcp6       0      0 ::1:631                 :::*                    LISTEN      -                   
tcp6       0      0 ::1:5432                :::*                    LISTEN      -                   
tcp6       0      0 :::46009                :::*                    LISTEN      -                   
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:34274           0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:36792           0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:68              0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:111             0.0.0.0:*                           -                   
udp        0      0 127.0.0.1:323           0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:45471           0.0.0.0:*                           -                   
udp        0      0 0.0.0.0:614             0.0.0.0:*                           -                   
udp        0      0 127.0.0.1:659           0.0.0.0:*                           -                   
udp6       0      0 :::54452                :::*                                -                   
udp6       0      0 :::5353                 :::*                                -                   
udp6       0      0 :::38815                :::*                                -                   
udp6       0      0 :::111                  :::*                                -                   
udp6       0      0 ::1:323                 :::*                                -                   
udp6       0      0 :::614                  :::*                                -                   
udp6       0      0 :::43712                :::*                                -                   

	
Conectarse desde otro pc como cliente

	iperf -c 172.16.99.22 

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

[root@j22 ~]# tshark -c 30
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp2s0'
  1 0.000000000 192.168.0.17 → 239.255.255.250 SSDP 213 M-SEARCH * HTTP/1.1 
  2 2.685674892 fe80::76d4:35ff:fece:5b65 → ff02::fb     MDNS 102 Standard query 0x0000 PTR _pgpkey-hkp._tcp.local, "QM" question
  3 2.685727915 192.168.4.26 → 224.0.0.251  MDNS 82 Standard query 0x0000 PTR _pgpkey-hkp._tcp.local, "QM" question
  4 2.973618948 172.16.99.21 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
  5 3.974617544 172.16.99.21 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
  6 4.975301138 172.16.99.21 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
  7 5.975978483 172.16.99.21 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
  8 7.735308898 fe80::96de:80ff:fe19:2d50 → ff02::fb     MDNS 180 Standard query 0x0000 PTR _ftp._tcp.local, "QM" question PTR _nfs._tcp.local, "QM" question PTR _afpovertcp._tcp.local, "QM" question PTR _smb._tcp.local, "QM" question PTR _sftp-ssh._tcp.local, "QM" question PTR _webdavs._tcp.local, "QM" question PTR _webdav._tcp.local, "QM" question
  9 7.735345731 192.168.1.17 → 224.0.0.251  MDNS 160 Standard query 0x0000 PTR _ftp._tcp.local, "QM" question PTR _nfs._tcp.local, "QM" question PTR _afpovertcp._tcp.local, "QM" question PTR _smb._tcp.local, "QM" question PTR _sftp-ssh._tcp.local, "QM" question PTR _webdavs._tcp.local, "QM" question PTR _webdav._tcp.local, "QM" question
 10 10.688791082 fe80::76d4:35ff:fece:5b65 → ff02::fb     MDNS 102 Standard query 0x0000 PTR _pgpkey-hkp._tcp.local, "QM" question
 11 10.688841394 192.168.4.26 → 224.0.0.251  MDNS 82 Standard query 0x0000 PTR _pgpkey-hkp._tcp.local, "QM" question
 12 14.630720251 192.168.0.19 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
 13 15.631817618 192.168.0.19 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
 14 16.633567009 192.168.0.19 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
 15 17.634682495 192.168.0.19 → 239.255.255.250 SSDP 214 M-SEARCH * HTTP/1.1 
 16 18.216909560 fe80::96de:80ff:fe19:283c → ff02::fb     MDNS 180 Standard query 0x0000 PTR _ftp._tcp.local, "QM" question PTR _nfs._tcp.local, "QM" question PTR _afpovertcp._tcp.local, "QM" question PTR _smb._tcp.local, "QM" question PTR _sftp-ssh._tcp.local, "QM" question PTR _webdavs._tcp.local, "QM" question PTR _webdav._tcp.local, "QM" question
 17 18.216937508 192.168.1.20 → 224.0.0.251  MDNS 160 Standard query 0x0000 PTR _ftp._tcp.local, "QM" question PTR _nfs._tcp.local, "QM" question PTR _afpovertcp._tcp.local, "QM" question PTR _smb._tcp.local, "QM" question PTR _sftp-ssh._tcp.local, "QM" question PTR _webdavs._tcp.local, "QM" question PTR _webdav._tcp.local, "QM" question
 18 23.976668950 192.168.3.22 → 192.168.0.10 TCP 66 693→2049 [ACK] Seq=1 Ack=1 Win=1444 Len=0 TSval=2870624 TSecr=3606946
 19 23.976814328 192.168.0.10 → 192.168.3.22 TCP 66 [TCP ACKed unseen segment] 2049→693 [ACK] Seq=1 Ack=2 Win=873 Len=0 TSval=3666946 TSecr=2810624
 20 24.136758405 192.168.3.22 → 192.168.0.10 NFS 274 [TCP Previous segment not captured] V4 Call SEQUENCE
 21 24.137258195 192.168.0.10 → 192.168.3.22 NFS 242 [TCP ACKed unseen segment] V4 Reply (Call In 20) SEQUENCE
 22 24.137300957 192.168.3.22 → 192.168.0.10 TCP 66 693→2049 [ACK] Seq=210 Ack=177 Win=1444 Len=0 TSval=2870784 TSecr=3667106
 23 26.696428944 fe80::76d4:35ff:fece:5b65 → ff02::fb     MDNS 102 Standard query 0x0000 PTR _pgpkey-hkp._tcp.local, "QM" question
 24 26.696459518 192.168.4.26 → 224.0.0.251  MDNS 82 Standard query 0x0000 PTR _pgpkey-hkp._tcp.local, "QM" question
 25 28.982448984 AsustekC_b1:f4:ed → Giga-Byt_49:7e:7c ARP 60 Who has 192.168.3.22? Tell 192.168.0.11
 26 28.982480116 Giga-Byt_49:7e:7c → AsustekC_b1:f4:ed ARP 42 192.168.3.22 is at 94:de:80:49:7e:7c
 27 30.726470078 fe80::428d:5cff:fee4:37c1 → ff02::fb     MDNS 101 Standard query 0x0000 PTR _nmea-0183._tcp.local, "QM" question
 28 30.726502075 192.168.2.52 → 224.0.0.251  MDNS 81 Standard query 0x0000 PTR _nmea-0183._tcp.local, "QM" question
 29 31.727581154 fe80::428d:5cff:fee4:37c1 → ff02::fb     MDNS 101 Standard query 0x0000 PTR _nmea-0183._tcp.local, "QM" question
 30 31.727633071 192.168.2.52 → 224.0.0.251  MDNS 81 Standard query 0x0000 PTR _nmea-0183._tcp.local, "QM" question
30 packets captured


Encontrar los 3 paquetes del handshake de tcp

 19 23.976814328 192.168.0.10 → 192.168.3.22 TCP 66 [TCP ACKed unseen segment] 2049→693 [ACK] Seq=1 Ack=2 Win=873 Len=0 TSval=3666946 TSecr=2810624
 20 24.136758405 192.168.3.22 → 192.168.0.10 NFS 274 [TCP Previous segment not captured] V4 Call SEQUENCE
 21 24.137258195 192.168.0.10 → 192.168.3.22 NFS 242 [TCP ACKed unseen segment] V4 Reply (Call In 20) SEQUENCE

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas
