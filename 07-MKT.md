EJERCICIO MIKROTIK 1.

OBJETIVO: FAMILIARIZARSE CON LAS OPERACIONES BÁSICAS EN UN ROUTER MIKROTIK

#1. Entrar al router con telnet, ssh y winbox

	TELNET: #telnet + ip del router  (coneccion no segura)
			se configura una contraseña para admin para luego
			acceder con ssh.
	
	SSH:	#ssh admin@ip router
			se introduce la misma contraseña que anteriormente

#2. Como resetaer el router

	Primero accedemos a system, i desde allí introducimos reset-configuration
	Tambien se puede por la parte de atras del router pulsando un botón escondido
	
	
#3. Cambiar nombre del dispositivo, password 

	Para cambiar el nombre tenemos que ir a system -> identity -> set name
	Para cambiar la contraseña solo tenemos que poner password
	
	
#4. Esquema de configuración de puertos inicial

	/interface print 
	Flags: D - dynamic, X - disabled, R - running, S - slave 
	#     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
	0     ether1                              ether            1500  1598       2028 6C:3B:6B:BC:C0:23
	1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:BC:C0:24
	2   S ether3                              ether            1500  1598       2028 6C:3B:6B:BC:C0:25
	3   S ether4                              ether            1500  1598       2028 6C:3B:6B:BC:C0:26
	4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:C0:27
	5  R  ;;; defconf
		bridge                              bridge           1500  1598            6C:3B:6B:BC:C0:24


/interface> ethernet set 2 master-port=none 


#5. Cambiar nombres de puertos y deshacer configuraciones iniciales

	/interfce -> set + nombre puerto + name + nombre que quieres poner -> puerto que quieres configurar
	hay otros modos tambien

#6. Backup y restauración de configuraciones

	Tanto para el backup como para restaurar hay que ir a 
	system -> backup -> load (para restaurar)