#Cisco 1700 
##(R17x14) Arnau Esteban 17/01/2017


System Bootstrap, Version 12.0(3)T, RELEASE SOFTWARE (fc1)
Copyright (c) 1999 by cisco Systems, Inc.
C1700 platform with 32768 Kbytes of main memory

program load complete, entry point: 0x80008000, size: 0x3d5bb4
Self decompressing the image : ##############################################################################################################################]

              Restricted Rights Legend

Use, duplication, or disclosure by the Government is
subject to restrictions as set forth in subparagraph
(c) of the Commercial Computer Software - Restricted
Rights clause at FAR sec. 52.227-19 and subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer
Software clause at DFARS sec. 252.227-7013.

           cisco Systems, Inc.
           170 West Tasman Drive
           San Jose, California 95134-1706



Cisco Internetwork Operating System Software 
IOS (tm) C1700 Software (C1700-Y-M), Version 12.2(4)XL, EARLY DEPLOYMENT RELEASE SOFTWARE (fc1)
TAC Support: http://www.cisco.com/tac
Copyright (c) 1986-2001 by cisco Systems, Inc.
Compiled Fri 16-Nov-01 17:49 by ealyon
Image text-base: 0x80008124, data-base: 0x80770B4C

cisco 1720 (MPC860T) processor (revision 0x601) with 24576K/8192K bytes of memory.
Processor board ID JAD05390DQM (1898849310), with hardware revision 0000
MPC860T processor: part number 0, mask 32
Bridging software.
X.25 software, Version 3.0.0.
1 FastEthernet/IEEE 802.3 interface(s)
2 Low-speed serial(sync/async) network interface(s)
32K bytes of non-volatile configuration memory.
8192K bytes of processor board System flash (Read/Write)








-----------------------------------------------------------------------

#comandos Router>:

	enable				--> superusuari
	configure terminal	--> superusuari
	
	no ip domain lookup --> no molestar el router
	hostname _____ 		--> cambiar el nom del superusuari
	
	
	## Desde enable:
	
		delete           Delete a file
		copy             Copy from one file to another
		erase            Erase a filesystem
		clear            Reset functions
		rename           Rename a file
		cd               Change current directory
		dir              List files on a filesystem
		mkdir            Create new directory
		more             Display the contents of a file


	para borrar la configuración del router: erase startup-config
	
	
	
	
	
	## Desde configure terminal:
	
		password                    Configure encryption password (key)
	
