port_trunk_conf="""
interface fa0/{port}
switchport mode trunk
switchport trunk allowed vlan {lloc}2-{lloc}6
switchport access vlan 200

"""

port_access_conf = """
interface fa0/{port}
switchport mode access
switchport access vlan {lloc}{num}
"""

port = 0
for lloc in [40,41]:
    port = port + 1
    print(port_trunk_conf.format(lloc=lloc,port=port))
    for num in range(2,7):
        port = port + 1
        print(port_access_conf.format(lloc=lloc,port=port,num=num))
