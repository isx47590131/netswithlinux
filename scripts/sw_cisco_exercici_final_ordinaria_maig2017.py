! cada switch se divide en 3 zonas para cada puesto de trabajo de cada fila
! primer puesto de la fila: 1 al 6
! segundo puesto de la fila:  7 al 12
! tercer puesto de la fila: 13 al 18

! para borrar la configuración inicial del switch:
! erase startup-config
! apagar el switch y reiniciarlo

! ejemplo con el puesto 29 que utiliza los puertos 7 al 12

!PUERTO DE ACCESO PARA dept A
interface fa0/1
switchport mode access
switchport access vlan 729

!PUERTO DE ACCESO PARA dept B
interface fa0/2
switchport mode access
switchport access vlan 829

!PUERTO DE ACCESO PARA servers
interface fa0/3
switchport mode access
switchport access vlan 929

!PUERTO DE TRUNK PARA mkt salida internet
interface fa0/4
switchport mode trunk
switchport trunk allowed vlan 1000,1005

!PUERTO DE TRUNK PARA mkt redes internas
interface fa0/5
switchport mode trunk
switchport trunk allowed vlan 729,829,929

!PUERTO HYBRIDO a la roseta
interface fa0/6
switchport mode access
switchport access vlan 1000



!PUERTO DE ACCESO PARA dept A
interface fa0/7
switchport mode access
switchport access vlan 728

!PUERTO DE ACCESO PARA dept B
interface fa0/8
switchport mode access
switchport access vlan 828

!PUERTO DE ACCESO PARA servers
interface fa0/9
switchport mode access
switchport access vlan 928

!PUERTO DE TRUNK PARA mkt salida internet
interface fa0/10
switchport mode trunk
switchport trunk allowed vlan 1000,1005

!PUERTO DE TRUNK PARA mkt redes internas
interface fa0/11
switchport mode trunk
switchport trunk allowed vlan 728,828,928

!PUERTO HYBRIDO a la roseta
interface fa0/12
switchport mode access
switchport access vlan 1000



no spanning-tree vlan 1-2000
	

